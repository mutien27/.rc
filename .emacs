;;;=== Set colors ===
; region background color
(set-face-background 'region "blue")

; emacs background color
(set-background-color "black")

; shell color
(autoload 'ansi-color-for-comint-mode-on "ansi-color" nil t)
(add-hook 'shell-mode-hook 'ansi-color-for-comint-mode-on)

;;;=== No backup files ===
(setq make-backup-files nil) 

;;;=== Set the fill column + auto-wrap ===
(setq-default fill-column 80)
(setq auto-fill-mode 1)

;;;=== Show line and column number ===
(require 'linum)
(global-linum-mode 1)

; optional formatting to make line numbers prettier
(setq linum-format "%d ")

; show column-number in the mode line
(column-number-mode 1)

;;;=== Disable automatic indent ===
(electric-indent-mode 0)

;;;=== Short cut ===
(global-set-key (kbd "C-x g") 'goto-line)
(global-set-key (kbd "C-x u") 'shell)
(global-set-key (kbd "C-o") 'other-window)
(global-set-key (kbd "C-u") 'backward-kill-sentence)
(defalias 'rs 'replace-string)
(defalias 'sir 'string-insert-rectangle)

;;;=== Python ===
; indent
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(ansi-color-names-vector
   ["#242424" "#e5786d" "#95e454" "#cae682" "#8ac6f2" "#333366" "#ccaa8f" "#f6f3e8"])
 '(custom-enabled-themes (quote (tango-dark)))
 '(python-guess-indent nil)
 '(python-indent 4)
 '(python-indent-guess-indent-offset nil)
 '(python-indent-offset 4))

;;;=== CUDA ===
; (setq auto-mode-alist (cons '("\\.cu\\'" . c++-mode) auto-mode-alist))

;;;=== Scons ===
(setq auto-mode-alist(cons '("SConstruct" . python-mode) auto-mode-alist))
(setq auto-mode-alist(cons '("SConscript" . python-mode) auto-mode-alist))

;;;=== Matlab and Octave ===
; (setq auto-mode-alist (cons '("\\.m$" . octave-mode) auto-mode-alist))

;;;=== Verilog ===
; (autoload 'verilog-mode "verilog-mode.el" "Verilog Mode" t)
; (setq auto-mode-alist (cons '("\\.v\\'" . verilog-mode) auto-mode-alist))
; (add-hook 'verilog-mode-hook '(lambda () (font-lock-mode 1)))

;;;=== C/C++ ===
(setq c-default-style "linux"
      c-basic-offset 4
      tab-width 4
      indent-tabs-mode t)
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

;;;=== Load .el example ===
; (add-to-list 'load-path' "~/.emacs.d/lisp/")
; (load "protobuf-mode.el")
; (require 'protobuf-mode)
